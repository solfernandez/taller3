#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov  7 15:05:28 2018

@author: sol
"""

import sys
import datetime

 
#%% Funciones

def promedios(n, data): 
    
    '''
    Crea una lista de listas con los promedios. 
    Cuando n > len(data) devuelve una lista vacia.
    '''
    avg_data = []
    for i in range(len(data)-n+1):
        avg_data_line = [[]]*(len(data[0])+1)
        for m in range(len(data[0])):
            suma = 0
            nans = 0
            for j in range(n):
                if data[j+i][m] == 'NA' or data[j+i][m] == 'NA\n': 
                    nans +=1
                else: 
                    suma += float(data[j+i][m])
            avg_data_line[0] = (tiempos[j+i]-tiempos[i]).total_seconds()
            if nans!= 0:
                avg_data_line[m+1] = 'NA'
            else:
                #avg_data_line[m+1] = format(round((suma/n),2),"%.2f")
                #avg_data_line[m+1] = format((suma/n),"%.2f")
                avg_data_line[m+1] = '{:.2f}'.format((suma/n))
        avg_data.append(avg_data_line)
    return avg_data

#%%  

#Parametros
input_file = sys.argv[1]
output_file = sys.argv[2]
n = int(sys.argv[3])


# Entrada y reacomodmiento de los datos
'''
Paso el archivo de entrada a data (una lista de listas con 
len(data) = cant lineas del archivo de entrada)
'''
entrada = open(input_file)
i = 0
data = []  
tiempos = [] #hago una lista con los tiempos como timestamps interpretados
for line in entrada:
    data.append(line.split(',')[1:len(line.split(','))])
    tiempos.append(datetime.datetime.strptime(line.split(',')[0], '%Y-%m-%dT%H:%M:%S'))


# Calculo de los promedios
avg_data = promedios(n, data)


# Escritura y salida de los datos finales
salida = open(output_file, 'w')
for line in avg_data:
    print(*line, sep = ',', file = salida)
salida.close()

# cuando avg_data es una lista vacia (cuando n>len(data)) el archivo de salida esta en blanco.


